//***************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20
//***************************************************** HTTPS server setup
//-----* Express inicia servidor / carpeta raiz
const express=require('express')
const app=express()

app.use(express.static(__dirname))

//-----* Filesystem module object
var fss=require('fs')
//-----* https module object
var https=require('https')
var key=fss.readFileSync('encryption/server.key')
var cert=fss.readFileSync('encryption/server.cert')
var httpsOptions={key:key,cert:cert}

const server=https.createServer(httpsOptions,app).listen(8888,function(connection){
	 console.log('Server ready...')  
})

server.on('data', function(data) { console.log(data.toString())})

//***************************************************** Seccion Socket 
var io = require('socket.io')(server)

io.on('connection', (socket)  => {

socket.on('ejemplo', async function (){await socketfunction()})

})//close io.on

//***************************************************** Postgres module object
const  {Client}  = require('pg')
postgresdb = new Client({
    host: 'localhost',
    user: 'app_aquiles',
    password: 'Redsamuraix1.',
    database: 'AquilesDB',
})

//***************************************************** Files Handler


//***************************************************** TCP/IP PLC TESLA
let plc_endresponse=0
io.on('connection', (socket) => {

	socket.on('plc_response', function(result_matrix){
		plcdatasender(result_matrix) 
	})
	
})

var net = require('net')
var tcpipserver = net.createServer(function(connection) { 
   console.log('TCP client connected')
   
   connection.write('Handshake ok!\r\n')
   
   connection.on('data', function(data) { io.emit('Timsequence_start',data.toString());console.log("Analisis in process...")})

//Responde a PLC cuando termine inspeccion
  setTimeout(function respuesta(){
	  estadoconexion = connection.readyState
	  console.log("Comunicacion con el plc :"+connection.readyState)
	  
		 if (estadoconexion == 'closed' ){
			 console.log("Puerto de PLC cerrado reintento en 1min..."  )
		 }
		 if (estadoconexion == 'open'){
				connection.write(plc_endresponse)
		 }

	},40000) // tiempo de secuencia completa 40s para responder
	})

function plcdatasender(result_matrix) {
	matrixtostring=result_matrix.toString()
	plc_endresponse=matrixtostring
}

tcpipserver.listen(40000, function() { 
   console.log('PLC Port 40000 listening...')
})